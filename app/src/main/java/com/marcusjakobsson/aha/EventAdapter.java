package com.marcusjakobsson.aha;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by christofferassgard on 2/4/18.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {
    private List<EventLog.Event> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View mHiddenView;
        public TextView mTimeTextView;
        public TextView mTextView;
        public ViewHolder(View v) {
            super(v);
            mHiddenView = v.findViewById(R.id.hidden_view);
            mTimeTextView = (TextView) v.findViewById(R.id.info_time);
            mTextView = (TextView) v.findViewById(R.id.info_text);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public EventAdapter(List<EventLog.Event> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public EventAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_row, parent, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (position == 0) {
            holder.mHiddenView.setVisibility(View.VISIBLE);
        }

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Stockholm"));
        calendar.setTimeInMillis(mDataset.get(position).timeInMillis);
        DateFormat format = DateFormat.getTimeInstance(DateFormat.SHORT);

        holder.mTimeTextView.setText(format.format(calendar.getTime()));
        String text = mDataset.get(position).text;
        if (mDataset.get(position).brushTime > 0)
            text += " (" + mDataset.get(position).brushTime + ").";
        holder.mTextView.setText(text);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
